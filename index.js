const getModuleData = require('./module-package');
const getModuleConfig = require('./get-module-config');

module.exports = {
    getModuleData,
    getModuleConfig,
};
